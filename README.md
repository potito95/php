# README

## Manual MarkDown
### Nivel 3

Este es el manua sobre la sintaxis MarkDown, al contrario que los editores como LibreOffice que son WYSIWYG.

### Negrita

Esto es una **negrita**.

### Cursiva
Esto es _cursiva_.


### Listas

**Listas**

* Elemento
* Elemento

**Lista ordenada:**

1. Uno
2. Dos
3. Tres

### Código
```php
<?php
echo "Hola mundo!"; 
?>

````
Código una sola linea así `echo "Hola mundo!"`;

### Enlaces
un enlace se hace asi [Texto de enlace]  https://gitlab.com/potito95/php.git


### Imagenes
Incrustar una imagen:

![texto alt](img/rus.png.png)


### Citas
> Esto es una cita de Marx.